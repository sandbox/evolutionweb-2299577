INTRODUCTION
------------
EvolutionWeb theme is a simple starter theme.
Responsive: MobileDetect library and CSS3 Media Query.
 * For a full description of the theme, visit the project page:
   https://www.drupal.org/sandbox/evolutionweb/2299577

REQUIREMENTS
------------
This theme requires the following library:
 * MobileDetect (http://mobiledetect.net)
 
INSTALLATION
------------
 * Download Theme
 * Download MobileDetect
 * Upload Theme to sites/all/themes
 * Upload MobileDetect to sites/all/libraries/mobiledetect/Mobile_Detect.php
 * Enable evolutionweb theme
   
CONFIGURATION
-------------
 * Configure CSS etc
