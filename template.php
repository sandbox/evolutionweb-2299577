<?php
/**
 * @file
 * Evolutionweb theme php logic.
 */

require_once 'sites/all/libraries/mobiledetect/Mobile_Detect.php';
$detect = new Mobile_Detect();
if (($detect->isMobile()) AND ($detect->isTablet() != TRUE)) {
  drupal_add_css(drupal_get_path('theme', 'evolutionweb') . '/css/phone.css', array('group' => CSS_THEME, 'type' => 'file'));
}
if ($detect->isTablet()) {
  drupal_add_css(drupal_get_path('theme', 'evolutionweb') . '/css/tablet.css', array('group' => CSS_THEME, 'type' => 'file'));
}
